import * as _ from 'lodash';
import * as SettingsManager from 'lib/settings_manager';
import config from 'lib/config';

/**
 * Save settings from DOM
 * @param e event handler
 * @return Promise<T>
 */
const saveSettings = async (e) => {
    await SettingsManager.save(await fromDOM());
};

/**
 * Parse DOM to retrieve user settings
 * @return [description]
 */
const fromDOM = async () => {
    const form = document.getElementById('nitterify-form') as HTMLFormElement;

    _.forEach(form, (e) => {
        if (e.tagName === 'INPUT') {
            config['app_settings']['instance'] = (e as HTMLInputElement).value;
        }
    });

    return config;
};

/**
 * Fill DOM with existing settings
 */
const toDOM = async () => {
    let settings = await SettingsManager.load();
    console.log(settings);

    _.forEach(settings.app_settings, (value, name) => {
        (document.getElementById(name) as HTMLInputElement).value = value;
    });
};

const init = async () => {
    try {
        await toDOM();
        let form = document.getElementById('nitterify-form') as HTMLFormElement;

        form.addEventListener('submit', (e) => { e.preventDefault(); });
        form.addEventListener('change', saveSettings);
    } catch (e) {
        console.error(e);
    }
};

document.addEventListener("DOMContentLoaded", init);
