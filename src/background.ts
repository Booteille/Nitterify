import * as browser from 'webextension-polyfill';
import * as SettingsManager from 'lib/settings_manager';
import config from 'lib/config';
import * as _ from 'lodash';

const nitterify = async (r) => {
    let url = new URL(r.url);

    if (!config.isEnabled) {
        return {};
    }

    if (url.hostname === 'video.twimg.com') {
        url.pathname = '/gif/' + encodeURIComponent(url.href);
    } else if (url.hostname === 'pbs.twimg.com') {
        url.pathname = '/pic/' + encodeURIComponent(url.href);
    }

    url.hostname = new URL((await SettingsManager.load())['app_settings']['instance']).hostname;

    return {
        redirectUrl: url.href
    };
};

const handleBackwardCompatibility = async (details) => {
    if (details.previousVersion <= '0.3.0') {
        config.app_settings.instance = (await SettingsManager.load())['instance'];

        await SettingsManager.save(config);
    }
};

const loadConfig = async () => {
    let c = await SettingsManager.load();

    _.forEach(c, (value, name) => {
        config[name] = value;
    });
};


const toggleIcon = async () => {
    await browser.browserAction.setTitle({ title: `${config.isEnabled ? 'Deactivate' : 'Activate'} Nitterify` });
    await browser.browserAction.setIcon({
        path: `assets/img/nitterify${!config.isEnabled ? '-disabled' : ''}.svg`
    });
};

const toggleEnabled = async () => {
    config.isEnabled = !config.isEnabled;
    await toggleIcon();
    await SettingsManager.save(config);
};

const run = async () => {
    // Handle button
    toggleIcon().then(r => browser.browserAction.onClicked.addListener(toggleEnabled));

    // Redirect Twitter requests
    browser.webRequest.onBeforeRequest.addListener(
        nitterify,
        { urls: [
                "*://mobile.twitter.com/*",
                "*://twitter.com/*",
                "*://video.twimg.com/*",
                "*://pbs.twimg.com/*"
            ] },
        ['blocking']
    );
};

// Handle backward compatibilities
browser.runtime.onInstalled.addListener(handleBackwardCompatibility);

loadConfig().then(() => run());

