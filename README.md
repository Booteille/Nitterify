# Nitterify - Twitter to Nitter Redirection

## Introduction

**Nitterify is deprecated in favor of [Invidition](https://addons.mozilla.org/firefox/addon/invidition/).**

Nitterify is a Web Extension redirecting Twitter links to Nitter, an alternative, privacy-friendly, interface.

This extension actually only redirect links from Twitter to Nitter and does not make any call to Twitter.
Embeds are not supported yet but will be once Nitter has matured.